const http = require('http').createServer();
const io = require('socket.io')(http);
const mydal = require('mssql');

http.listen(9085);

var config = {
    user: '',
    password: '',
    server: '', 
    database: '' 
};


mydal.connect(config,(err)=>{
    
  if(err) console.log(err);

});

let openDataBuy = [] ;
let openDataSell = [] ;


const getMarketOrder = () => {

	return new Promise((resolve,reject) => {

		const sql = new  mydal.Request();
  		sql.stream=true;

  		sql.query(`select top 1 *  from Market_Order_Process where market_send=0 and id>(select last_process_id from Market_Last (nolock) )`);

	    sql.on('recordset', columns => {
	        // Emitted once for each recordset in a query   
	    })

	    sql.on('row', row => {

	    	console.log(row.id);
	    	
	    	 new mydal.Request()
		    	.query(`update Market_Order_Process set market_send=1 where id=${row.id} `, (err, result) => {

		    	})

	    	 new mydal.Request()
		    	.query(`update market_last set last_process_id=${row.id} `, (err, result) => {

			 })
			
			io.emit('DoneOrder',row);
	      	//console.log(row); 

	      	resolve();

	    })

	})
}



io.on('connection', socket => {


	socket.on('Refreh', data => {

    	console.log(data.from+"-"+data.to+" Refreh");
    	console.log('--------------');


		const sql = new  mydal.Request();
  		sql.stream=true;

  		sql.query(`sp_Bekleyen_Hareketler_Toplu_Refresh_Alis '${data.from}', '${data.to}' `);

	    sql.on('recordset', columns => {
	        // Emitted once for each recordset in a query   
	    })

	    sql.on('row', row => {
	      //console.log(row);
	      openDataBuy.push(row);
	    })
 
	    sql.on('error', err => {
	        console.log(err);
	    })
 
	    sql.on('done', result => {
	        socket.emit("refresh_data", openDataBuy ); 
	        openDataBuy=[];
	    })

    
	    const sql1 = new  mydal.Request();
	    sql1.stream=true;

	    sql1.query(`sp_Bekleyen_Hareketler_Toplu_Refresh_Satis '${data.from}', '${data.to}' `);

	    sql1.on('recordset', columns => {
	        // Emitted once for each recordset in a query   
	    })

	    sql1.on('row', row => {
	      //console.log(row);
	      openDataSell.push(row);
	    })

	    sql1.on('error', err => {
	        console.log(err);
	    })

	    sql1.on('done', result => {
	        socket.emit("refresh_data_sell", openDataSell ); 
	        openDataSell=[];
	    })

	})


	socket.on('Order', () => {

		const sql = new  mydal.Request();
  		sql.stream=true;

  		sql.query(`select * from Market_Order_Process where market_send=0 and id>(select last_process_id from Market_Last (nolock) )`);

	    sql.on('recordset', columns => {
	        // Emitted once for each recordset in a query   
	    })

	    sql.on('row', row => {

	    	console.log(row.id);
	    	
	    	 new mydal.Request()
		    	.query(`update Market_Order_Process set market_send=1 where id=${row.id} `, (err, result) => {

		    	})

	    	 new mydal.Request()
		    	.query(`update market_last set last_process_id=${row.id} `, (err, result) => {

			 })
			
			io.emit('DoneOrder',row);
	      	//console.log(row); 

	    })
		
	})



	/**
	async function intervalFunc() {
	  	
	  	const order = await getMarketOrder();

	}

	setInterval(intervalFunc, 1000);**/


})


